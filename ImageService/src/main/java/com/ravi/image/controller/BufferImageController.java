package com.ravi.image.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ravi.image.bufferimagedao.BufferImageDao;
import com.ravi.image.entity.Image;
import com.ravi.image.util.UtilHelper;

@Controller
@RequestMapping(value="/bufferimage")
public class BufferImageController {
	@Autowired()
	private BufferImageDao bufferdao;
	
	@RequestMapping(value="/saveimage",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String,Object>> saveImage(@RequestParam("file") MultipartFile file) throws IOException{
		byte[] bytes = file.getBytes();
		Map<String,Object> map = new HashMap<String,Object>();
		System.out.println("entered");
		long id = UtilHelper.getTimeInMillis();
		Image image =  new Image();
		image.setImage(bytes);
		Image kk = bufferdao.save(image);
		map.put("url", "bufferimage/"+kk.getId());
		map.put("status", true);
		return ResponseEntity.ok().body(map);
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.GET,produces = MediaType.IMAGE_PNG_VALUE)
	@ResponseBody
	public byte[] getImageData(@PathVariable String id){
		Image img = bufferdao.findOne(Long.valueOf(id));
		return img.getImage();
	}
	@RequestMapping(value="/updateimage/{id}",method=RequestMethod.POST)
	public ResponseEntity<Map<String,Object>> updateImage(@RequestParam("file") MultipartFile file,@PathVariable String id) throws IOException{
		byte[] bytes = file.getBytes();
		Map<String,Object> map = new HashMap<String,Object>();
		Image image = bufferdao.findOne(Long.valueOf(id));
		image.setImage(bytes);
		Image kk = bufferdao.save(image);
		map.put("url", "bufferimage/"+kk.getId());
		map.put("status", true);
		return ResponseEntity.ok().body(map);
	}
}
