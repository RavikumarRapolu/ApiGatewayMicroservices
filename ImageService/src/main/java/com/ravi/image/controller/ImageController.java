package com.ravi.image.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/image")
public class ImageController {

	private static String UPLOADED_FOLDER = "src\\main\\java\\com\\ravi\\image\\controller\\images\\";

	@RequestMapping(value = "/uploadsingle", method = RequestMethod.POST, headers = ("content-type=multipart/*"))
	@ResponseBody
	public ResponseEntity<Map<String, Object>> uploadImage(@RequestParam("file") MultipartFile file) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (file.isEmpty()) {
			map.put("status", false);
			map.put("data", null);
			return ResponseEntity.ok().body(map);
		}
		List<String> result = saveImagestoFolder(Arrays.asList(file));
		map.put("status", true);
		map.put("data", result.get(0));
		return ResponseEntity.ok().body(map);
	}

	@RequestMapping(value = "/uploadmultiple", method = RequestMethod.POST, headers = ("content-type=multipart/*"))
	@ResponseBody
	public ResponseEntity<Map<String, Object>> uploadImage(@RequestParam("file") List<MultipartFile> files) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (files.size() == 0) {
			map.put("status", false);
			map.put("data", null);
			return ResponseEntity.ok().body(map);
		}
		List<String> result = saveImagestoFolder(files);
		map.put("status", true);
		map.put("data", result);
		return ResponseEntity.ok().body(map);
	}

	private List<String> saveImagestoFolder(List<MultipartFile> files) {
		int count = 0;
		List<String> list = new ArrayList<String>();
		for (MultipartFile file : files) {
			try {
				byte[] bytes = file.getBytes();
				String date = getTimeInMillis();
				Path path = Paths.get(UPLOADED_FOLDER + date + ".png");
				Files.write(path, bytes);
				count++;
				list.add("images\\" + date + ".png");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		if (count == files.size()) {
			return list;
		} else {
			return list;
		}
	}

	private synchronized String getTimeInMillis() {
		return String.valueOf(new Date().getTime());
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public String getHello() {
		return "Hello image Api is working";
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public @ResponseBody byte[] getImage(@PathVariable String id) throws IOException {
		String url = "images\\" + id + ".png";
		System.out.println("url "+url);
		InputStream in = getClass().getResourceAsStream(url);
		return IOUtils.toByteArray(in);
	}
}
