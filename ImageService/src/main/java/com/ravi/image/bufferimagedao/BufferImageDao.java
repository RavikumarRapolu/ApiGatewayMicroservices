package com.ravi.image.bufferimagedao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ravi.image.entity.Image;

public interface BufferImageDao extends PagingAndSortingRepository<Image, Long> {
	
}
