package com.ravi.main.security;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import static java.util.Collections.emptyList;

public class TokenAuthenticationService {
	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "RaviRapolu";
	static final String TOKEN_PREFIX = "Bearer";
	static final String HEADER_STRING = "Authorization";
	static final String USERNAME="Username";

	static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader(HEADER_STRING);
		String username =request.getHeader(USERNAME);
		System.out.println("username "+username);
		if (token != null) {
			// parse the token.
			String user = Jwts.parser()
					.setSigningKey(SECRET)
					.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody()
					.getSubject();
			System.out.println("user "+user);
			return (user != null && user.equals(username))?
					new UsernamePasswordAuthenticationToken(user, null, emptyList()) :
						null;
		}
		return null;
	}
}
