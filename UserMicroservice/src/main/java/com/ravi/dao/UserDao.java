package com.ravi.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;
import com.ravi.model.User;

@Transactional
public interface UserDao extends Repository<User, Long> {

	Page<User> findAll(Pageable pageRequest);

	User save(User user);

	List<User> findByNameAndEmail(String name,String email);
	List<User> findByemail(String email);
	

}
