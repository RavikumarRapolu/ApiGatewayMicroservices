package com.ravi.userserviceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ravi.dao.UserDao;
import com.ravi.model.User;
import com.ravi.service.Userservice;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UserServiceImpl implements Userservice {
	static final long EXPIRATIONTIME = 864_000_000; // 10 days
	static final String SECRET = "RaviRapolu";
	@Autowired(required = false)
	private UserDao userDao;

	@Override
	public Map<String, Object> login(User user) {
		System.out.println("login..");
		Map<String, Object> map = new HashMap<String, Object>();
		List<User> fetcheduser = userDao.findByemail(user.getEmail());
		if (fetcheduser.size() == 0) {
			map.put("success", true);
			map.put("data", null);
			map.put("status", HttpStatus.NO_CONTENT);
		} else {
			if (fetcheduser.get(0).getPassword().equals(user.getPassword())) {
				String token = Jwts.builder()
				        .setSubject(fetcheduser.get(0).getEmail())
				        .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				        .signWith(SignatureAlgorithm.HS512, SECRET)
				        .compact();
				map.put("data", fetcheduser);
				map.put("success", true);
				map.put("token", token);
				map.put("status", HttpStatus.OK);
			} else {
				map.put("data", null);
				map.put("success", true);
				map.put("status", HttpStatus.NOT_ACCEPTABLE);
			}
		}
		return map;
	}

}
