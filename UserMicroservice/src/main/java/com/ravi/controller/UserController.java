package com.ravi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ravi.dao.UserDao;
import com.ravi.model.User;
import com.ravi.service.Userservice;

@Controller
public class UserController {

	@Autowired(required = false)
	private UserDao userDao;
	
	@Autowired(required=false)
	private Userservice userservice;

	@RequestMapping(value = "/user/save", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> save(@RequestBody User user) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		System.out.println("user "+user.getDepartmentid());
		User user1 = userDao.save(user);
		
		map.put("data", user1);
		return ResponseEntity.ok().body(map);
	}

	@RequestMapping(value = "/user/getall", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getAll(Pageable pageRequest) {
		Map<String, Object> map = new HashMap<String, Object>();
		Page<User> value = userDao.findAll(pageRequest);
		map.put("success", true);
		map.put("data", value);
		return ResponseEntity.ok().body(map);
	}

	@RequestMapping(value = "/user/getbyname", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getByName(@RequestParam String name, @RequestParam String email) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("success", true);
		List<User> user = userDao.findByNameAndEmail(name, email);
		System.out.println(user);
		map.put("data", user);
		return ResponseEntity.ok().body(map);
	}
	
	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> login(@RequestBody User user) {
		Map<String, Object> res = userservice.login(user);
		return ResponseEntity.ok().body(res);
	}
	}

