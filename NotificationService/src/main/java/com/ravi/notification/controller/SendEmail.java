package com.ravi.notification.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@SuppressWarnings({ "deprecation", "unused" })
@Controller
@RequestMapping(value="/email")
public class SendEmail {

	@Autowired
	private JavaMailSender sender;

	@Autowired
	private VelocityEngine velocityEngine;

	@RequestMapping(value = "/sendemail/{to}/{text}", method = RequestMethod.GET)
	@ResponseBody
	public String sendEmailService(@PathVariable String to, @PathVariable String text) {
		try {
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,true);
			helper.setTo(to);
			helper.setText(text);
			helper.setSubject("Hi");
			ClassPathResource file = new ClassPathResource("login.png");
			helper.addAttachment("keka", file);

			sender.send(message);
			return "Email Sent!";
		} catch (Exception ex) {
			return "Error in sending email: " + ex;
		}

	}

	@RequestMapping(value = "/sendmultipart/{to}/{text}", method = RequestMethod.GET)
	@ResponseBody
	public String sendMultipartEmailService(@PathVariable String to, @PathVariable String text) {
		try {
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo(to);
			helper.setText("<html><body>Here is a cat picture!<br><br> <img src='cid:id101'/><body></html>", true);
			helper.setSubject("Hi");
			ClassPathResource file = new ClassPathResource("login.png");
			// helper.addAttachment("keka", file);
			helper.addInline("id101", file);

			sender.send(message);
			return "Email Sent!";
		} catch (Exception ex) {
			return "Error in sending email: " + ex;
		}

	}

	@RequestMapping(value = "/sendvelocity", method = RequestMethod.POST)
	@ResponseBody
	public String sendVelocityEmail(@RequestBody Map<String, String> map) throws MessagingException {
		String to = map.get("to");
		String body = map.get("body");
		try {
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("user", map.get("user"));
			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "welcome.vm", model);
			helper.setTo(to);
			helper.setText(text, true);
			helper.setSubject("Welcome USer");
			sender.send(message);
			return "Success";
		} catch (Exception e) {
			return "Error is  " + e;
		}
	}

}
